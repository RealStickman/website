# Updating Containers

Last Edited 28.03.2021  

## Update
*Example Jellyfin*  
`$ podman pull docker://jellyfin/jellyfin`  

**Make backups**  

Stop the old container  
`$ podman stop jellyfin`  

Start new container  
```
podman run -d \
    --name jellyfin-new \
    --cgroup-manager=systemd \
    --volume jellyfin-config:/config \
    --volume jellyfin-cache:/cache \
    --volume (media path):/media \
    --net=host
    jellyfin/jellyfin
```

Verify everything is working  

### Remove old and rename new container
Remove old container  
`$ podman rm jellyfin`  

Rename new container  
`$ podman rename jellyfin-new jellyfin`  

`$ podman start jellyfin`  

## Downgrade
*Example Jellyfin*  
`$ podman stop jellyfin-new`  

`$ podman start jellyfin`  
