# All Services on one machine

Last Edited: 09.02.2021  

## Website
The website will run on nginx  
`# apt install nginx certbot python-certbot-nginx`  

Sample configuration with ssl stuff included  
```
server {
    root /var/www/website; #change this for different path to website directory
    index index.html; #name of main file
    server_name DOMAIN_NAME; #this is what certbot will make certificates for
    location / {
       try_files $uri $uri/ =404;
     }

    listen [2a01:4f8:191:2236::10]:443 ssl; #set ipv6 address
    ssl_certificate /etc/letsencrypt/live/DOMAIN_NAME/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/DOMAIN_NAME/privkey.pem;
    include /etc/letsencrypt/options-ssl-nginx.conf;
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;



    add_header Strict-Transport-Security "max-age=31536000" always;


    ssl_trusted_certificate /etc/letsencrypt/live/DOMAIN_NAME/chain.pem;
    ssl_stapling on;
    ssl_stapling_verify on;



}

server {
    if ($host = DOMAIN_NAME) {
        return 301 https://$host$request_uri;
    } # managed by Certbot


    listen [2a01:4f8:191:2236::10]:80; #ipv6, to specify an address use [(address)]:(port)
    root /var/www/website; #change this for different path to website directory
    index index.html; #name of main file
    server_name DOMAIN_NAME; #this is what certbot will make certificates for
    location / {
       try_files $uri $uri/ =404;
     }
}
```

Enable the config  
`$ ln -s /etc/nginx/sites-available/(config) /etc/nginx/sites-enabled/`  

Restart nginx  
`# systemctl restart nginx`  

## Nextcloud
Nextcloud will be using apache  
`# apt install mlocate apache2 libapache2-mod-php mariadb-client mariadb-server wget unzip bzip2 curl php php-common php-curl php-gd php-mbstring php-mysql php-xml php-zip php-intl php-apcu php-redis php-http-request python-certbot-apache php-bcmath php-gmp php-imagick`  

No password set  
`# mariadb -u root -p`  

`CREATE DATABASE nextcloud;`  

`GRANT ALL ON nextcloud.* TO 'nextcloud'@'localhost' IDENTIFIED BY '<password>';`  

`FLUSH PRIVILEGES;`  

Exit the MariaDB prompt  

Download Nextcloud into `/var/www`  
`$ wget https://download.nextcloud.com/server/releases/nextcloud-(version).tar.bz2`  

`$ tar -xf nextcloud-(version)`  

Change owner to nextcloud user  
`# chown -Rfv www-data:www-data /var/www/nextcloud`  

Create nextcloud configuration for apache  
`# vi /etc/apache2/sites-available/nextcloud.conf`  

Configuration file  
```
<VirtualHost *:80> #specify listen ip addresses: (address):(port) for ipv4, [(address)]:(port) vor ipv6, *:80 for all
ServerAdmin webmaster@localhost
DocumentRoot /var/www/nextcloud
Alias /nextcloud "/var/www/nextcloud/"
    
<Directory "/var/www/nextcloud/">
Options +FollowSymlinks
AllowOverride All

<IfModule mod_dav.c>
Dav off
</IfModule>

Require all granted

SetEnv HOME /var/www/nextcloud
SetEnv HTTP_HOME /var/www/nextcloud
</Directory>

ErrorLog ${APACHE_LOG_DIR}/nextcloud_error_log
CustomLog ${APACHE_LOG_DIR}/nextcloud_access_log common
</VirtualHost>
```

Enable nextcloud and disable the default site  
`# a2ensite nextcloud.conf && a2dissite 000-default.conf`  

Edit `ports.conf` for apache2 to only bind the addresses you need  

`# systemctl restart apache2`  

## Jellyfin
`# apt install apt-transport-https`  

Version with sudo  
`$ wget -O - https://repo.jellyfin.org/jellyfin_team.gpg.key | sudo apt-key add -`  

`$ echo "deb [arch=$( dpkg --print-architecture )] https://repo.jellyfin.org/$( awk -F'=' '/^ID=/{ print $NF }' /etc/os-release ) $( awk -F'=' '/^VERSION_CODENAME=/{ print $NF }' /etc/os-release ) main" | sudo tee /etc/apt/sources.list.d/jellyfin.list`

Version without sudo  
`$ wget -O - https://repo.jellyfin.org/jellyfin_team.gpg.key | apt-key add -`  

`$ echo "deb [arch=$( dpkg --print-architecture )] https://repo.jellyfin.org/$( awk -F'=' '/^ID=/{ print $NF }' /etc/os-release ) $( awk -F'=' '/^VERSION_CODENAME=/{ print $NF }' /etc/os-release ) main" | tee /etc/apt/sources.list.d/jellyfin.list`

`# apt update`  

`# apt install jellyfin`  

Create a new nginx configuration file in `/etc/nginx/sites-available`  
*Make sure to replace "DOMAIN\_NAME" with your domain name*  
```
server {
    server_name DOMAIN_NAME;

    # use a variable to store the upstream proxy
    # in this example we are using a hostname which is resolved via DNS
    # (if you aren't using DNS remove the resolver line and change the variable to point to an IP address e.g `set $jellyfin 127.0.0.1`)
    set $jellyfin 127.0.0.1;
    #resolver 127.0.0.1 valid=30;

    # Security / XSS Mitigation Headers
    add_header X-Frame-Options "SAMEORIGIN";
    add_header X-XSS-Protection "1; mode=block";
    add_header X-Content-Type-Options "nosniff";

    location = / {
        return 302 https://$host/web/;
    }

    location / {
        # Proxy main Jellyfin traffic
        proxy_pass http://$jellyfin:8096;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-Protocol $scheme;
        proxy_set_header X-Forwarded-Host $http_host;

        # Disable buffering when the nginx proxy gets very resource heavy upon streaming
        proxy_buffering off;
    }

    # location block for /web - This is purely for aesthetics so /web/#!/ works instead of having to go to /web/index.html/#!/
    location = /web/ {
        # Proxy main Jellyfin traffic
        proxy_pass http://$jellyfin:8096/web/index.html;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-Protocol $scheme;
        proxy_set_header X-Forwarded-Host $http_host;
    }

    location /socket {
        # Proxy Jellyfin Websockets traffic
        proxy_pass http://$jellyfin:8096;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-Protocol $scheme;
        proxy_set_header X-Forwarded-Host $http_host;
    }

    listen [2a01:4f8:191:2236::2]:443 ssl; #set ipv6 address
    ssl_certificate /etc/letsencrypt/live/DOMAIN_NAME/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/DOMAIN_NAME/privkey.pem;
    include /etc/letsencrypt/options-ssl-nginx.conf;
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;
}

server {
    if ($host = DOMAIN_NAME) {
        return 301 https://$host$request_uri;
    }

    listen [2a01:4f8:191:2236::2]:80; #set ipv6 address
    server_name DOMAIN_NAME;
    return 404;

}
```

## Matrix
`# apt install build-essential python3-dev libffi-dev python3-pip python3-setuptools sqlite3 libssl-dev python3-virtualenv libjpeg-dev libxslt1-dev certbot python-certbot-nginx nginx`  

Preparing virtual environment  
`# mkdir -p /etc/synapse`  
`# virtualenv -p python3 /etc/synapse/env`  
`# source /etc/synapse/env/bin/activate`  

Install python packages  
`# pip3 install --upgrade pip virtualenv six packaging appdirs setuptools matrix-synapse`  

`$ cd /etc/synapse`  

Set synapse configuration  
*Make sure to replace "DOMAIN\_NAME" with your domain name*  
```
python -m synapse.app.homeserver \
  --server-name DOMAIN_NAME \
  --config-path homeserver.yaml \
  --generate-config \
  --report-stats=no
```

Edit `/etc/synapse/homeserver.yaml`  
Search for `bind_addresses:` and set it to `['127.0.0.1']`  

Create an nginx config file `/etc/nginx/sites-available/matrix`
*Make sure to replace "DOMAIN\_NAME" with your domain name*  
```
server {
    listen 80; #ipv4 address
	listen [::]:80; #ipv6 address
    server_name DOMAIN_NAME;
    return 301 https://$host$request_uri;
}

server {
    listen 443 ssl; #ipv4 address
    listen [::]:443 ssl; #ipv6 address
    server_name DOMAIN_NAME;

    ssl on;
    ssl_certificate /etc/letsencrypt/live/DOMAIN_NAME/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/DOMAIN_NAME/privkey.pem;

    location / {
        proxy_pass http://localhost:8008;
        proxy_set_header X-Forwarded-For $remote_addr;
    }
}

server {
    listen 8448 ssl default_server; #ipv4 address
    listen [::]:8448 ssl default_server; #ipv6 address
    server_name DOMAIN_NAME;

    ssl on;
    ssl_certificate /etc/letsencrypt/live/DOMAIN_NAME/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/DOMAIN_NAME/privkey.pem;
    location / {
        proxy_pass http://localhost:8008;
        proxy_set_header X-Forwarded-For $remote_addr;
    }
}
```

`$ ln -s /etc/nginx/sites-available/(config) /etc/nginx/sites-enabled/`  

Restart nginx  

`# cd /etc/synapse`  
`# source /etc/synapse/env/bin/activate`  
`# synctl start`  

Create new user through interactive dialogue  
`# register_new_matrix_user -c homeserver.yaml http://localhost:8008`  

## WikiJS
### Preparation
Create a new network for the database and wikijs  
`$ podman network create wikijs`  

### Database setup
`$ podman pull docker://postgres`  

`$ podman run --net wikijs -p 127.0.0.1:5432:5432 --name wikijsdb -e POSTGRES_PASSWORD=wikijs -d postgres`  

`$ podman exec -it wikijsdb bash`  

`$ psql -U postgres`  

Create database used by wikijs  
`=# CREATE DATABASE wikijs;`  

### Wiki.JS Setup
`# apt install nodejs`  

`$ cd /var`  
`# wget https://github.com/Requarks/wiki/releases/download/2.5.201/wiki-js.tar.gz`  
`# mkdir wiki`  
`# tar xzf wiki-js.tar.gz -C ./wiki`  
`$ cd ./wiki`  

Move default config  
`# mv config.sample.yml config.yml`  
```
#######################################################################
# Wiki.js - CONFIGURATION                                             #
#######################################################################
# Full documentation + examples:
# https://docs.requarks.io/install

# ---------------------------------------------------------------------
# Port the server should listen to
# ---------------------------------------------------------------------

port: 3000

# ---------------------------------------------------------------------
# Database
# ---------------------------------------------------------------------
# Supported Database Engines:
# - postgres = PostgreSQL 9.5 or later
# - mysql = MySQL 8.0 or later (5.7.8 partially supported, refer to docs)
# - mariadb = MariaDB 10.2.7 or later
# - mssql = MS SQL Server 2012 or later
# - sqlite = SQLite 3.9 or later

db:
  type: postgres

  # PostgreSQL / MySQL / MariaDB / MS SQL Server only:
  host: localhost
  port: 5432
  user: postgres
  pass: wikijs
  db: wikijs
  ssl: false

  # Optional - PostgreSQL / MySQL / MariaDB only:
  # -> Uncomment lines you need below and set `auto` to false
  # -> Full list of accepted options: https://nodejs.org/api/tls.html#tls_tls_createsecurecontext_options
  sslOptions:
    auto: true
    # rejectUnauthorized: false
    # ca: path/to/ca.crt
    # cert: path/to/cert.crt
    # key: path/to/key.pem
    # pfx: path/to/cert.pfx
    # passphrase: xyz123

  # SQLite only:
  storage: path/to/database.sqlite

#######################################################################
# ADVANCED OPTIONS                                                    #
#######################################################################
# Do not change unless you know what you are doing!

# ---------------------------------------------------------------------
# SSL/TLS Settings
# ---------------------------------------------------------------------
# Consider using a reverse proxy (e.g. nginx) if you require more
# advanced options than those provided below.

ssl:
  enabled: false
  port: 3443

  # Provider to use, possible values: custom, letsencrypt
  provider: custom

  # ++++++ For custom only ++++++
  # Certificate format, either 'pem' or 'pfx':
  format: pem
  # Using PEM format:
  key: path/to/key.pem
  cert: path/to/cert.pem
  # Using PFX format:
  pfx: path/to/cert.pfx
  # Passphrase when using encrypted PEM / PFX keys (default: null):
  passphrase: null
  # Diffie Hellman parameters, with key length being greater or equal
  # to 1024 bits (default: null):
  dhparam: null

  # ++++++ For letsencrypt only ++++++
  domain: wiki.yourdomain.com
  subscriberEmail: admin@example.com

# ---------------------------------------------------------------------
# Database Pool Options
# ---------------------------------------------------------------------
# Refer to https://github.com/vincit/tarn.js for all possible options

pool:
  # min: 2
  # max: 10

# ---------------------------------------------------------------------
# IP address the server should listen to
# ---------------------------------------------------------------------
# Leave 0.0.0.0 for all interfaces

bindIP: 0.0.0.0

# ---------------------------------------------------------------------
# Log Level
# ---------------------------------------------------------------------
# Possible values: error, warn, info (default), verbose, debug, silly

logLevel: info

# ---------------------------------------------------------------------
# Offline Mode
# ---------------------------------------------------------------------
# If your server cannot access the internet. Set to true and manually
# download the offline files for sideloading.

offline: false

# ---------------------------------------------------------------------
# High-Availability
# ---------------------------------------------------------------------
# Set to true if you have multiple concurrent instances running off the
# same DB (e.g. Kubernetes pods / load balanced instances). Leave false
# otherwise. You MUST be using PostgreSQL to use this feature.

ha: false

# ---------------------------------------------------------------------
# Data Path
# ---------------------------------------------------------------------
# Writeable data path used for cache and temporary user uploads.
dataPath: ./data
```

Don't forget to open permissions so the systemd service can run the server  
*Note: Recommended to create a "wiki" user who owns this folder*  
`# chmod 777 -R /var/wiki`  

Run server directly:  
`$ node server`  

### Systemd service
Put this under `/etc/systemd/system/wiki.service`  
```
[Unit]
Description=Wiki.js
After=network.target

[Service]
Type=simple
ExecStart=/usr/bin/node server
Restart=always
# Consider creating a dedicated user for Wiki.js here:
User=nobody
Environment=NODE_ENV=production
WorkingDirectory=/var/wiki

[Install]
WantedBy=multi-user.target
```

`# systemctl daemon-reload`  
`# systemctl enable --now wiki`  

### Nginx config
```
server {
    server_name DOMAIN_NAME;

    # Security / XSS Mitigation Headers
    add_header X-Frame-Options "SAMEORIGIN";
    add_header X-XSS-Protection "1; mode=block";
    add_header X-Content-Type-Options "nosniff";

    location = / {
        return 302 https://$host/web/;
    }

    location / {
        # Proxy main traffic
        proxy_pass http://127.0.0.1:3000;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-Protocol $scheme;
        proxy_set_header X-Forwarded-Host $http_host;
    }

    listen [2a01:4f8:191:2236::50]:443 ssl; #set ipv6 address
    ssl_certificate /etc/letsencrypt/live/DOMAIN_NAME/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/DOMAIN_NAME/privkey.pem;
    include /etc/letsencrypt/options-ssl-nginx.conf;
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;
}

server {
    if ($host = DOMAIN_NAME) {
        return 301 https://$host$request_uri;
    }

    listen [2a01:4f8:191:2236::50]:80; #set ipv6 address
    server_name DOMAIN_NAME;
    return 404;
}
```

## HTTPS
For nginx  
`# certbot --nginx --agree-tos --redirect --hsts --staple-ocsp --email (email) -d (domain1),(domain2)`  
Use certonly with Website, Jellyfin, Matrix and WikiJS  
`# certbot certonly ...`  

For apache  
`# certbot --apache --agree-tos --redirect --hsts --staple-ocsp --email (email) -d (domain1),(domain2)`  

Enable cron timer for renew  
`$ crontab -e`  

```
1 1 1 * * certbot renew
```
