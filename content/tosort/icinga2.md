# Icinga2

Last Edited: 19.03.2021  

## Setup Server
`# apt install apache2 mariadb-server mariadb-client`  

Enable the root account to log in  
`# mysql`  
`mysql> ALTER USER root@localhost IDENTIFIED VIA mysql_native_password;`  
`mysql> SET PASSWORD = PASSWORD('(password)');`  
`# systemctl restart mariadb`  

`# wget -O - https://packages.icinga.com/icinga.key | apt-key add -`  

Edit `/etc/apt/sources.list.d/icinga.list`  
For debian 10 (buster)  
```
deb http://packages.icinga.com/debian icinga-buster main
deb-src http://packages.icinga.com/debian icinga-buster main
```

`# apt update`  

`# apt install icinga2 icinga2-ido-mysql`  

You will be presented with a configuration dialog for icinga2-ido-mysql.  
1. Select `YES`
2. Select `YES`
3. Enter a password

`# icinga2 feature enable ido-mysql command`  

`# systemctl restart icinga2`  

## Setup Webinterface
`# apt install icingaweb2`  

Edit `/etc/php/(version)/apache2/php.ini`  
```
date.timezone = Europe/Zurich
```

`# systemctl restart apache2`  

For the setup in the webinterface we need a setup token  
`# icingacli setup token create`  

Go to `http://(ip/domain)/icingaweb2` for the web setup  

### Database Setup
![](./icinga2_pic1_database.png)  

As the database does not yet exist, you have to enter `root` and its password to create that database  
For the rest of the installation, the defaults are fine  

### Monitoring Setup
![](./icinga2_pic2_monitoring_db.png)  

Next we have to make sure `Command Transport` is set to `Local Command File`  
![](./icinga2_pic3_command_transport.png)  

For `Monitoring Security` the defaults are fine  

Finally we get an overview of our settings  

## Monitoring Master
`# icinga2 node wizard`  

You will be asked a few questions to set everything up  
1. Answer `N` to create a master
2. Enter the hostname
3. Accept the defaults for the rest of the questions

`# systemctl restart icinga2`  

### Add new node
`# icinga2 pki ticket --cn '(hostname)'`  

## Linux Client Setup
`# apt install icinga2`  

`# icinga2 node wizard`  

You will be asked a few questions to set everything up  
1. Answer `Y` to create a client
2. Enter the hostname
3. Enter the hostname of the master server
4. Answer `Y`
5. Enter the master server IP
6. `BLANK`
7. `N`
8. Confirm whether the information is correct `Y`
9. Enter the ticket number created on the master
10. `BLANK`
11. `BLANK`
12. `Y`
13. `Y`
14. Local zone name
15. Master zone name
16. `N`
17. `Y`

`# systemctl restart icinga2`  

## Configuration file
Each client has to have a matching configuration file on the master server  

*Example:*  
`/etc/icinga2/zones.d/master/(client name).conf`  
```
// Endpoints & Zones
object Endpoint "(client name)" {
}

object Zone "(client name)" {
     endpoints = [ "(client name)" ]
     parent = "master"
}

// Host Objects
object Host "(client name)" {
    check_command = "hostalive"
    address = "(client ip)"
    vars.client_endpoint = name //follows the convention that host name == endpoint name

// Custom Optional Check - END
}
```
*client name example: deb10-client.home*  

## Services
`/etc/icinga2/zones.d/master/services.conf`  
```
// Ping Check
apply Service "Ping" {
  check_command = "ping4"
  assign where host.address // check is executed on the master node
}

// System Load
apply Service "System Load" {
  check_command = "load"
  command_endpoint = host.vars.client_endpoint // Check executed on client node
  assign where host.vars.client_endpoint
}

// System Process Count
apply Service "Process" {
  check_command = "procs"
  command_endpoint = host.vars.client_endpoint
  assign where host.vars.client_endpoint
}

// Logged in User Count
apply Service "Users" {
  check_command = "users"
  command_endpoint = host.vars.client_endpoint
  assign where host.vars.client_endpoint
}

// Disk Usage Check
apply Service "Disk" {
  check_command = "disk"
  command_endpoint = host.vars.client_endpoint
  assign where host.vars.client_endpoint
}

// Disk Usage Check for Specific Partition
apply Service for (disk => config in host.vars.local_disks) {
  check_command = "disk"
  vars += config
  command_endpoint = host.vars.client_endpoint
  assign where host.vars.client_endpoint
}

// System Swap Check
apply Service "SWAP" {
  check_command = "swap"
  command_endpoint = host.vars.client_endpoint
  assign where host.vars.client_endpoint
}

// SSH Service Check
apply Service "SSH Service" {
  check_command = "ssh"
  command_endpoint = host.vars.client_endpoint
  assign where host.vars.client_endpoint
}

// Icinga 2 Service Check
apply Service "Icinga2 Service" {
  check_command = "icinga"
  command_endpoint = host.vars.client_endpoint
  assign where host.vars.client_endpoint
}

// Apache VirtualHost Check
apply Service for (http_vhost => config in host.vars.local_http_vhosts) {
  check_command = "http"
  vars += config
  command_endpoint = host.vars.client_endpoint
  assign where host.vars.client_endpoint
}

// TCP Port Check
apply Service for (tcp_port => config in host.vars.local_tcp_port) {
  check_command = "tcp"
  vars += config
  display_name = + vars.service_name + " - " + vars.port_number
  command_endpoint = host.vars.client_endpoint
  assign where host.vars.client_endpoint
}
```

`# icinga2 daemon -c`  
`# systemctl restart icinga2`  
