# DNS Config

Last Edited: 04.12.2020  

## Set different DNS

### Add DNS Server

1. Go to System -> General Setup -> DNS Server Settigs. Here any DNS server can be added  
2. Make sure to disable "DNS Server Override"  

### Use added DNS Servers

1. Go to Services -> DNS Resolver -> General Settings  
2. Enable "DNS Query Forwarding"  

