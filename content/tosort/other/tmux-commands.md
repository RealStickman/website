# Useful commands for tmux

Last Edited: 08.12.2020  

## Windows

### Create Window

`CTRL b c`  

### Switch Windows

To go to the next window:  
`CTRL b n`  

To go to the previous window:  
`CTRL b p`  

Go to a specific window:  
`CTRL b (0-9)`  

## Sessions

### Create new Session

`CTRL b :`  
Enter `new` for an unnamed and `new -s (name)` for a named session  

### Switch Sessions

`CTRL b s`  

### Delete Sessions

Close current session  
`CTRL b : kill-session`  

Close all other sessions  
`CTRL b : kill-session -a`  

Kill specific session  
`CTRL b : kill-session -t (session id)`  

### Detatch Session

`CTRL b d`  
