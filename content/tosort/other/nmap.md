# Nmap

Last Edited: 21.12.2020  

## Scanning network

Scan ports in the current network  
`$ nmap -sT (network address/cidr)`  

*Example:*  
`$ nmap -sT 192.168.1.0/24`  

*Example specific range:*  
`$ nmap -sT 192.168.1.10-30`  

List all devices responding to ping on network  
`$ nmap -sn (network address/cidr)`  

OS detection  
`# nmap -sT -O 192.168.1.0/24`  
