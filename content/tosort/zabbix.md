# Zabbix

Last Edited: 19.03.2021  

## Setup Server

### Apache2 and MySQL  
`$ wget https://repo.zabbix.com/zabbix/(release)/debian/pool/main/z/zabbix-release/zabbix-release_(release)-1+buster_all.deb`  

`# dpkg -i zabbix-release_(release)-1+buster_all.deb`  `# dpkg -i zabbix-release_5.0-1+buster_all.deb`  

`# apt update`  

`# apt install zabbix-server-mysql zabbix-frontend-php zabbix-apache-conf zabbix-agent mariadb-server`  

`# mysql -uroot -p`  
`mysql> create database zabbix character set utf8 collate utf8_bin;`  
`mysql> create user zabbix@localhost identified by '(password)';`  
`mysql> grant all privileges on zabbix.* to zabbix@localhost;`  

Quit mysql again  

`$ cd /usr/share/doc/zabbix-server-mysql*`  

Import data to database  
`$ zcat create.sql.gz | mysql -uzabbix -p zabbix `  

Set the database name, user and password in `/etc/zabbix/zabbix_server.conf`  
`DBName=(database)`  
`DBUser=(user)`  
`DBPassword=(password)`  

Set the correct timezone in `/etc/zabbix/apache.conf`  
`php_value date.timezone Europe/Zurich`  

Start and enable needed services  
`# systemctl restart zabbix-server zabbix-agent apache2`  
`# systemctl enable zabbix-server zabbix-agent apache2`  

The frontend can be reached on `http://(ip/domain)/zabbix`  
Follow the steps to finalise setup  
The default user is `Admin` with the password `zabbix`  

### Nginx and MySQL  
`$ wget https://repo.zabbix.com/zabbix/(release)/debian/pool/main/z/zabbix-release/zabbix-release_(release)-1+buster_all.deb`  

`# dpkg -i zabbix-release_(release)-1+buster_all.deb`  

`# apt update`  

`# apt install zabbix-server-mysql zabbix-frontend-php zabbix-nginx-conf zabbix-agent mariadb-server`  

`# mysql -uroot -p`  
`mysql> create database zabbix character set utf8 collate utf8_bin;`  
`mysql> create user zabbix@localhost identified by '(password)';`  
`mysql> grant all privileges on zabbix.* to zabbix@localhost;`  

Quit mysql again  

`$ cd /usr/share/doc/zabbix-server-mysql*`  

Import data to database  
`$ zcat create.sql.gz | mysql -uzabbix -p zabbix`  

Set the database name, user and password in `/etc/zabbix/zabbix_server.conf`  
`DBName=(database)`  
`DBUser=(user)`  
`DBPassword=(password)`  

Edit `/etc/zabbix/nginx.conf` and set `listen` and `server_name`  

Set timezone in `/etc/zabbix/php-fpm.conf`  
`php_value[date.timezone] = Europe/Zurich`  

Disable the default site in nginx  

Start and enable services  
`# systemctl restart zabbix-server zabbix-agent nginx php7.3-fpm`
`# systemctl enable zabbix-server zabbix-agent nginx php7.3-fpm`

The frontend can be reached on `http://(ip/domain)`  
Follow the steps to finalise setup  
The default user is `Admin` with the password `zabbix`  

## Setup Clients

### Debian
`$ wget https://repo.zabbix.com/zabbix/(release)/debian/pool/main/z/zabbix-release/zabbix-release_(release)-1+buster_all.deb`  

`# dpkg -i zabbix-release_(release)-1+buster_all.deb`  

`# apt update`  

`# apt install zabbix-agent`  

Edit `/etc/zabbix/zabbix_agentd.conf`  
`Server=(controlling server ip)`  
`ServerActive=(controlling server ip)`  

If you have a firewall, allow port 10050  

`# systemctl restart zabbix-agent`  
`# systemctl enable zabbix-agent`  

### Windows
Download agent for windows corresponding to your zabbix version  
