# Android Backups

Last Edited: 02.04.2021  

## ADB
Start the ADB Server  
`# adb start-server`  

List devices  
`$ adb devices`  

Create full backup  
`$ adb backup -f "(path)/backup-(year)-(month)-(day)" -apk -shared -all`  

### Restore Backup
`$ adb restore (backup)`  

## References
[ADB Tutorial on Github](https://gist.github.com/AnatomicJC/e773dd55ae60ab0b2d6dd2351eb977c1)  
