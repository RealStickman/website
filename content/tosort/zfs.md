# ZFS

Last Edited: 16.02.2021  

## Module and Utils
`$ paru -S zfs-dkms`  

A lot of stuff has to be enabled now so ZFS mounts its drives automatically on every reboot.  
`# systemctl enable zfs-import-cache.service`
`# systemctl enable zfs-import.target`
`# systemctl enable zfs-mount.service`
`# systemctl enable zfs.target`

Create the file `/etc/modules-load.d/zfs.conf`  
```
# load zfs kernel module
zfs
```

## Commands

### Create Pool

#### with Mirrored vdev (RAID 1)

#### with RaidZ vdev (RAID 5/6)
