# NFS Client on Windows

Last Edited: 18.01.2021  

## Installation

Search for `Turn Windows features on or off`  

Check everything under `Services for NFS` and click "OK"  

Mount as mapped network drive  
`mount -o anon \\(ip)\(mountpoint) (letter):`  
