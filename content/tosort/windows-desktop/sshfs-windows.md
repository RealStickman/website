# SSHFS on Windows

Last Edited: 18.01.2021  

## Client

Install [WinFSP](https://github.com/billziss-gh/winfsp)  

Install [sshfs-win](https://github.com/billziss-gh/sshfs-win)  

## Usage

*No path = start in remote user's home directory*  

### GUI

Map a new network drive in Windows Explorer  
`\\sshfs\(user)@(ip/domain)\(path)`  

### Terminal

Mount drive  
`net use (letter): \\sshfs\(user)@(ip/domain)\(path)`  

Show mounted drives  
`net use`  

Remove mounted drive  
`net use (letter): /delete`  

## Server

Install the openssh server on Windows  
[OpenSSH on Windows](/content/windows-desktop/ssh-windows.html)  

## References

[sshfs-win](https://github.com/billziss-gh/sshfs-win)  
