# RDP Client on Windows

Last Edited: 05.01.2021  

## Configuration

Enter `Remote Desktop Connection` in Windows search.  

The target computer can be specified by IP or name  

After clicking on `connect` the user will be asked to insert the username and password.  

### Use different port

![](./rdp-winpro-client-pic1-example-port.png)  

## References

[Linux: RDP client configuration](/content/linux-desktop/rdp-linux-client.html)  
[Linux: RDP server configuration](/content/linux-server/debian/rdp-linux-server.html)  
[Windows Server: RDP server configuration](/content/windows-server/rdp-winser-server.html)  
[Windows Pro: RDP server configuration](/content/windows-desktop/rdp-winpro-server.html)  


