# Public/Private Network switch

Last Edited: 10.12.2020  

## Changing Security Policy

Press `Super+R`  

Enter `secpol.msc`  

Go to "Network List Manager Policies", select the Network you want to use and set "Network Location" to Private.  

