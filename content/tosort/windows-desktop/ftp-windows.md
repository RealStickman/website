# FTP Server on Windows

Last Edited: 13.01.2021  

## Installation

Search for `Turn Windows features on or off`  

Under `Internet Information Services` check `FTP Server`  

Under `FTP Server` select `FTP Extensibility` as well  

In `Internet Information Services` check `Web Management Tools`  

## Setup

Open the `Internet Information Services (IIS) Manager`  

Rightclick on the Hostname and select `Add FTP Site`  

Give the Site a name and choose the path it goes to.  

Next disable SSL. Everything else can be left on default.  

*SSL requires setup and shoud be turned on in a company setting*  

For Authentication choose `basic` as well as `Specified users` with Read and Write permissions.  

Make sure to enter the users you want to have access.  

To let the FTP traffic through the firewall, go to `Windows Firewall` and select `Allow an app or feature through Windows Firewall`.  

Select `FTP Server` and check both `Private` and `Public`  

Reboot the PC and FTP should work.  
