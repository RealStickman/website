# Common CSS

Last Edited: 08.12.2020  

## Documentation

[Reference Documentation](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Properties_Reference)  

## Reset Stylesheet

Dies sollte immer am Anfang einer CSS Datei stehen.  
```
* {
    padding:0;
    margin:0;
}
```

## Center object

`margin: 0 auto;`  

