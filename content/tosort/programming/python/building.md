# python building for pypi

Last Edited: 16.11.2020  

## Package creation

Create a setup.py file.  

Execute in main program directory.  
```bash
python setup.py bdist_wheel sdist
```

Upload to pypi  
```bash
twine upload --skip-existing dist/*
```
