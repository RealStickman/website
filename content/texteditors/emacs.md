# Doom Emacs Shortcuts

Last Edited: 19.04.2021  

## Keybindings

### Minimap
`SPC t m`  

### Dired
Provides directory view  

Create new directory within the current directory  
`Shift +`  

Create new file in current directory  
`SPC . <enter new file name>`  

Delete files or directories  
`d`, `x`  

Unselect  
`u`  

### Treemacs
Toggle view of directory structure of the current project on the side.  
`SPC o p`  

### Term
Open terminal  
`SPC o t`  

### Window management
Open window right of current window  
`SPC w v`  

Open window below current window  
`SPC w s`  

Move to other windows  
`SPC h/j/k/l`  

### Buffers
Open recent within the same project buffers  
`SPC b b`  
`SPC ,`  

Remove buffers  
`SPC b k`  

Open new empty buffer  
`SPC b N`  

Save buffer  
`SPC b s`  

### Quickly move to start/end of a document
Start of document  
`gg`  

End of document  
`G`  

### Evil Snipe
Move to next occurence of one letter  
`f (letter)`  

Move to previous occurence of one letter  
`F (letter)`  

`;` continue in that direction  
`,` go in the opposite direction  

`s (letter)` or `S (letter)` for occurences of two letters  

### Indent selection
Press `CTRL x` followed by `TAB` and use h/l to indent text  

### SSH Editing
`SPC f f`  
Enter `/ssh:`  
Press `TAB` to show available options  
Enter new options with the following syntax: `/ssh:root@albedo.realstickman.net:/`  

#### Privilege elevation
Execute sudo after establishing the connection  
`/ssh:nonroot@albedo.realstickman.net|sudo:nonroot@albedo.realstickman.net:/`  

## Windows installation

### git
Go to the [git homepage](https://git-scm.com/) and install it.  

### emacs
Go to the [emacs homepage](https://www.gnu.org/software/emacs/) and install it.  
Add the `(location)\emacs\x86_84\bin` directory to your PATH in the environment variables.  

#### Shortcut
Create a shortcut to `(location)\emacs\x86_64\bin\runemacs.exe`  
Edit the shortcut to execute in your home directory `C:\Users\(user)`  

### HOME
Add the path to your home to the environment variables.  

New variable -> HOME -> `C:\Users\(user)`  

### doom-emacs
Open git bash  
```bash
git clone --depth 1 https://github.com/hlissner/doom-emacs ~/.emacs.d
```
```bash
~/.emacs.d/bin/doom install
```

Add `C:\Users\(user)\.emacs.d\bin` to your PATH.  

*Currently doesn't show emotes*  
*Missing ripgrep and fd*  
