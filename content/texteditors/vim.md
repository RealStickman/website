# Vim

Last Edited: 15.02.2021  

## Get output from command
`:r!(command)`  

*Example to get UUID for a disk*  
`:r!blkid /dev/(partition) -sUUID -ovalue`  

