# RDP on Linux

Last Edited: 05.01.2021  

## Configuration

`# apt install xrdp`  

`# systemctl enable xrdp`  

Put the desktop environment you want to start in `.xsession`  

*Example*  
`xfce4-session`  

`# systemctl restart xrdp`  

### Change port

Edit `/etc/xrdp/xrdp.ini`  

Change the value of `port` to what you want  

`# systemctl restart xrdp`

## References

[Linux: RDP client configuration](/content/linux-desktop/rdp-linux-client.html)  
[Windows Pro: RDP client configuration](/content/windows-desktop/rdp-winpro-client.html)  
[Windows Server: RDP server configuration](/content/windows-server/rdp-winser-server.html)  
[Windows Pro: RDP server configuration](/content/windows-desktop/rdp-winpro-server.html)  

### External

[Azure RDP configuration](https://docs.microsoft.com/en-us/azure/virtual-machines/linux/use-remote-desktop)  
[ArchWiki xrdp](https://wiki.archlinux.org/index.php/Xrdp)  

