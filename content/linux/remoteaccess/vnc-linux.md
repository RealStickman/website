# VNC Server and Client on Linux

Last Edited: 05.01.2021  

## Installation

Install Remmina with libvncserver to get client functionality.  
`# pacman -S remmina libvncserver`  

For the VNC Server we will be using tightVNC.  
`# apt install tightvncserver`  

## Client

Use Remmina as Client  

![](./vnc-linux/vnc-linux-pic1-example.png)  

## Server

Use tightVNC as Server  

Initial setup and starting VNC server  
`vncserver`  

You will have to enter a password  

Optionally, a view-only password can be created as well.  

Kill VNC server  
`vncserver -kill :1`  

Edit the `xstartup` file in `.vnc` to your liking.  

*Example with xfce*  
```
#!/bin/sh

xrdb $HOME/.Xresources
unset SESSION_MANAGER
unset DBUS_SESSION_BUS_ADRESS
exec startxfce4
```

### Change password

`vncpasswd`  

You can also add a view-only password  
