# fstab

Last Edited: 26.12.2020  

## Other drives

Find uuid with `sudo blkid`  
`UUID=(uuid) (mountpath) (filesystem) defaults,noatime 0 2`  

## Samba shares

`//(ip)/(path)/ (mountpath) cifs uid=0,credentials=(path to credentials file),iocharset=utf8,noperm,nofail 0 0`  

Example credentials file:  
```
user=(user)
password=(password)
domain=WORKGROUP
```
Make sure to set permissions to the credential files to something like 700.  
