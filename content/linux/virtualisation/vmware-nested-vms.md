# Proxmox under VMWare

Last Edited: 12.01.2021  

## Networking for nested VMs

To pass through the network connection to nested VMs, the first VM has to put the network adapter into promiscuous mode.  
By default only root is allowed to do that, however the permissions can also be granted to others.  

Grant permission to group:  
```
# chgpr (group) /dev/vmnetX
# chmod g+rw /dev/vmnetX
```

Grant permission to everyone:  
`# chmod a+rw /dev/vmnetX`  

## Allow nested VMs

Enable the following two settings under "Processor" in the settings of the VM.  
`Virtualize Intel VT-x/EPT or AMD-V/RVI`  
`Virtualize CPU performance counters`  

## Fix MSRS bug on Ryzen CPUs

Add `kvm.ignore_msrs=1` in `/etc/default/grub` to `GRUB_CMDLINE_LINUX_DEFAULT=`  
Update the Grub configuration  
`# grub-mkconfig -o /boot/grub/grub.cfg`  
