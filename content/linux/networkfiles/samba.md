# Samba Setup

Last Edited: 25.12.2020  

## Setup

Login as sudoer  
`sudo apt install samba smbclient`  

samba conf backup  
`sudo cp /etc/samba/smb.conf /etc/samba/smb.conf_backup`  

*Samba users have to exist on the system as well before they are added to samba's user management system.*  

Add user to samba and create a password for it  
`sudo smbpasswd -a (user)`  

Directories can be shared with groups or users.  
Make sure to [set the owner and group](/content/linux-other/files.html) for the directories you want to share.  

### Sharing with users

```
[sharename]
   path = (absolute path)
​   read only = no
​   writeable = yes
​   browseable = yes
​   valid users = (user), (user 2), (user 3)
​   create mask = 0660
​   directory mask = 0770
```

### Sharing with groups

Make sure to add all users to the group  
The "@" signals samba that this is a group  
```
[sharename] 
   path = (absolute path)
​   read only = no
​   writeable = yes
​   browseable = yes
​   valid users = @(group)
​   create mask = 0660
​   directory mask = 0770
```

Finally, restart the samba service.  
`sudo systemctl restart smbd`  
