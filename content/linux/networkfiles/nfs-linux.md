# NFS on Linux

Last Edited: 18.01.2021  

## Server

`# apt install nfs-kernel-server`  

Shares can be configured in `/etc/exports`  

`(mountpoint)   (allowed_ip)(options) (allowed_ip2)(options)`  

### Options

```
ro: specifies that the directory may only be mounted as read only
rw: grants both read and write permissions on the directory
no_root_squash: is an extremely dangerous option that allows remote “root” users the same privilege as the “root” user of the host machine
subtree_check: specifies that, in the case of a directory is exported instead of an entire filesystem, the host should verify the location of files and directories on the host filesystem
no_subtree_check: specifies that the host should not check the location of the files being accessed withing the host filesystem
sync: this just ensures that the host keeps any changes uploaded to the shared directory in sync
async: ignores synchronization checks in favor of increased speed
```

*Example single host:*  
`/mnt/nfs   192.168.1.123(rw,sync,no_subtree_check)`  

*Example whole subnet:*  
`/mnt/nfs   192.168.1.0/24(rw,sync,no_subtree_check)`  

Apply new config by restarting the service.  
`# systemctl restart nfs-kernel-server`  

Show configured shares  
`$ cat /var/lib/nfs/etab`  

## Client

`# pacman -S nfs-utils`  

Mount through terminal  
`# mount -t nfs4 (ip):(mountpoint) (local mountpoint)`

Can also be mounted with fstab  
