# FTP Server on Linux

Last Edited: 13.01.2021  

## Configuration

Install ftp server  
`# apt install vsftpd`  

Append the following lines to `/etc/vsftpd.conf`  
```
pasv_enable=Yes
pasv_min_port=10000
pasv_max_port=10100
allow_writeable_chroot=YES
```

Restart service  
`# systemctl restart vsftpd`  
