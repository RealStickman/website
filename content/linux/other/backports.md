# Add buster backports  

Last Edited: 16.11.2020  

## Buster-backports

Appends the appropriate line to the apt sources list.  
`? echo 'deb http://deb.debian.org/debian buster-backports main contrib non-free' >> /etc/apt/sources.list  `  
