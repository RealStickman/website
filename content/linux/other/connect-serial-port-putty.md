# Putty

Last Edited: 16.11.2020  

## Finding and connecting to serial port

1. Find proper serial port using `ls /dev | grep tty` or `ls -l /sys/class/tty`  
2. Connect to it using `# putty /dev/(port) -serial -sercfg (baud rate),8,n,1,N`  

