# Join Active Directory

Last Edited: 05.02.2021  

## Join Domain
*Doesn't work for some reason*  
`# apt install sssd-ad sssd-tools realmd adcli`  

Confirm if the realm is reachable  
`# realm -v discover lad.realstickman.net`  

Join domain  
`# realm join -v lad.realstickman.net`  

You will be asked to input the password for the "Administrator" account on the domain.  
Use "-U" if you want to specify another user  

### Automatic home directory creation
`# pam-auth-update --enable mkhomedir`  
