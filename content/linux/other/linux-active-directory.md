# Active Directory

Last Edited: 05.02.2021  

## Installation
`# apt install samba smbclient krb5-user winbind`  

## Configuration
`# mv /etc/samba/smb.conf /etc/samba/smb.conf.old`  

Stop unneeded services  
`# systemctl stop smbd nmbd winbind`  

Disable unneeded services  
`# systemctl disable smbd nmbd winbind`  

Mask services  
`# systemctl mask smbd nmbd winbind`  

Make ad dc service available  
`# systemctl unmask samba-ad-dc`  

Enable ad dc service  
`# systemctl enable samba-ad-dc`  

## Domain Creation
/*
Interactive setup for domain  
`# samba-tool domain provision --use-rfc2307 --interactive`  

```
Realm: Use domain name in caps. e.g. LAD.REALSTICKMAN.NET
Domain: Give your realm a name. e.g. LADREAL
Server Role: Choose "dc" for primary domain controller
DNS Backend: Choose SAMBA_INTERNAL unless you need something else
DNS Forwarder IP Address: Choose your DNS Server for non-local queries
Administrator Password: Choose a strong password
```
*/

Example of doing this in one command  
`# samba-tool domain provision --use-rfc2307 --server-role=dc --dns-backend=SAMBA_INTERNAL --realm="LAD.REALSTICKMAN.NET" --domain="ladreal" --adminpass="password1$"`  

Make a backup of the old kerberos config  
`# mv /etc/krb5.conf /etc/krb5.conf.old`  

Copy the kerberos config created to `/etc/krb5.conf`  
The path to the config that was created should be the last output of the provisioning command  
![](linux-active-directory-pic1-kerberos-conf.png)  

`# systemctl start samba-ad-dc`  

## DNS
Set a DNS override for the chosen domain in your DNS server  

/*
`# apt install resolvconf`  

Create `/etc/resolv.conf.tail`  
```
# Samba configuration
search (domain)
# nameservers
nameserver ::1
nameserver 127.0.0.1
```

`# chmod 644 /etc/resolv.conf.tail`  

Update resolver  
`# resolvconf -u`  
*/

## Verifying services

### DNS
`# host -t A (hostname).lad.realstickman.net`  

`$ host -t SRV _ldap._tcp.lad.realstickman.net`  
`$ host -t SRV _kerberos._tcp.lad.realstickman.net`  
`$ host -t SRV _kerberos._udp.lad.realstickman.net`  

### Kerberos
`$ kinit administrator`  

`$ klist`  

### File Server
`$ smbclient -L localhost -U%`  

`$ smbclient //localhost/netlogon -U Administrator -c 'ls'`  

Everything is now ready to join clients to your Active Directory domain.  

## Create new user
`# samba-tool user create (name) --uid-number=(num) --nis-domain=lad.realstickman.net --login-shell="/bin/bash" --unix-home="/home/(name)@lad.realstickman.net --gid-number=(num)`  

## List users
`# samba-tool user list`  
