#!/bin/bash
set -euo pipefail

# argument #1: $1

file="$1"
# name of inputfile
echo "Modifying $file"
filename="${file%.*}"
#echo "$filename"
newfile="${filename}.html"
#echo "$newfile"

pandoc -i "${file}" -o "${newfile}"

# inserting heading and leading stuff
sed -i '1i<!DOCTYPE html>' "${newfile}"
sed -i '2i<html lang="en">' "${newfile}"
sed -i '3i<head>' "${newfile}"
# Needed to fix problems with quotation marks in text
sed -i '4i<meta charset="UTF-8">' "${newfile}"
sed -i '5i<title>RealStickman</title>' "${newfile}"
# NOTE: This only works on a webserver. Not locally!
sed -i '6i<link rel="stylesheet" href="/css/main.css">' "${newfile}"
sed -i '7i<link rel="icon" href="/lowpolygirl.png">' "${newfile}"
sed -i '8i<meta name="viewport" content="width=device-width, initial-scale=1">' "${newfile}"
sed -i '9i</head>' "${newfile}"
sed -i '10i<body>' "${newfile}"
sed -i '11i<section class="section">' "${newfile}"
sed -i '12i<a href="/index.html"><span class="icon"><i class="fas fa-home"></i></span></a>\n' "${newfile}"

# make title 1 a class=title is-1
sed -i 's/^<h1\s/<h1 class="title is-1" /' "${newfile}"

# adds div class="box" after "Last Edited"
#sed -i '/^<p>Last\sEdited:\s.*/a <div class="box">' "${newfile}"

# make title-2 a class=title is-2
sed -i 's/^<h2\s/<h2 class="title is-2" /' "${newfile}"

# insert /div and div class="box"
sed -i '/^<h2\sclass="title\sis-2"/i </div>\n<div class="box">' "${newfile}"

# make title-3 a class=title is-3
sed -i 's/^<h3\s/<h3 class="title is-3" /' "${newfile}"

# make title-4 a class=title is-4
sed -i 's/^<h4\s/<h4 class="title is-4" /' "${newfile}"

# make title-5 a class=title is-5
sed -i 's/^<h5\s/<h5 class="title is-5" /' "${newfile}"

# make title-6 a class=title is-6
sed -i 's/^<h6\s/<h6 class="title is-6" /' "${newfile}"

# insert stuff at the end of the file
echo '</div>' >> "${newfile}"
echo '</section>' >> "${newfile}"
echo '</body>' >> "${newfile}"
echo '</html>' >> "${newfile}"

# final output
echo "Finished $newfile"
